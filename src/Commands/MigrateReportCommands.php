<?php

namespace Drupal\migrate_report\Commands;

use Drupal\migrate_report\MigrateReportHelper;
use Drush\Commands\DrushCommands;

/**
 * Provides Drush commands to generate the migrate report.
 */
class MigrateReportCommands extends DrushCommands {

  /**
   * The migrate report helper service.
   *
   * @var \Drupal\migrate_report\MigrateReportHelper
   */
  protected $migrateReportHelper;

  /**
   * Constructs a new Drush commands instance.
   *
   * @param \Drupal\migrate_report\MigrateReportHelper $migrate_report_helper
   *   The migrate report helper service.
   */
  public function __construct(MigrateReportHelper $migrate_report_helper) {
    parent::__construct();
    $this->migrateReportHelper = $migrate_report_helper;
  }

  /**
   * Generate a report based on last migration.
   *
   * The file is saved in directory configured via UI. Alternatively
   * --destination=/path/to/destination can be used to pass the report location.
   *
   * @command migrate:report-generate
   *
   * @option destination
   *   If passed, the report will be placed in that directory. Supports also
   *   Drupal stream wrappers.
   *
   * @usage migrate:report-generate
   *   Generate a .txt file in the UI configured directory
   * @usage mrg --destination=/path/to/reports
   *   Generate a .txt file in /path/to/reports directory
   *
   * @validate-module-enabled migrate
   *
   * @aliases mrg,migrate-report-generate
   */
  public function reportGenerate(array $options = ['destination' => NULL]) {
    $path = $options['destination'];

    if (($reason = $this->migrateReportHelper->canGenerate($path)) !== TRUE) {
      $message = dt('The report cannot be generated due to following reasons:') . "\n" . implode("\n", $reason);
      $this->logger()->warning($message);
    }

    if ($file = $this->migrateReportHelper->generate($path)) {
      $this->logger()->notice(dt("Generated report: @report.", ['@report' => $file]));
    }
    else {
      $this->logger()->error(dt('Error generating report'));
    }
  }

}
