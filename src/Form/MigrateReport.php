<?php

namespace Drupal\migrate_report\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Url;
use Drupal\migrate_report\MigrateReportHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for migrate report.
 */
class MigrateReport extends FormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The migrate report helper service.
   *
   * @var \Drupal\migrate_report\MigrateReportHelper
   */
  protected $migrateReportHelper;

  /**
   * Constructs a new form instance.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager service.
   * @param \Drupal\migrate_report\MigrateReportHelper $migrate_report_helper
   *   The migrate report helper service.
   */
  public function __construct(FileSystemInterface $file_system, StreamWrapperManagerInterface $stream_wrapper_manager, MigrateReportHelper $migrate_report_helper) {
    $this->fileSystem = $file_system;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->migrateReportHelper = $migrate_report_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('file_system'),
      $container->get('stream_wrapper_manager'),
      $container->get('migrate_report.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_report.config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('migrate_report.config');

    $can_generate = $this->migrateReportHelper->canGenerate();
    if (is_array($can_generate)) {
      $this->messenger()->addWarning([
        '#theme' => 'item_list',
        '#items' => $can_generate,
        '#title' => $this->t('The report cannot be generated due to following reasons:'),
      ]);
    }

    $form['container'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Generate report'),
      '#description' => $this->t('A text report will be generated in the %dir directory. Note that the report is based on the last migration run.', ['%dir' => $config->get('report_dir')]),
    ];
    $form['container']['generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate'),
      '#disabled' => $can_generate !== TRUE,
    ];

    $report_dir = $config->get('report_dir');
    $files = [];
    foreach ($this->fileSystem->scanDirectory($report_dir, '/\.txt$/', ['key' => 'name']) as $key => $file) {
      if ($this->streamWrapperManager->isValidUri($file->uri)) {
        $files[$key] = Link::fromTextAndUrl($file->filename, Url::fromUri(file_create_url($file->uri)));
      }
      else {
        $files[$key] = $file->filename;
      }
    }
    krsort($files);

    $form['report'] = [
      '#theme' => 'item_list',
      '#items' => $files,
      '#empty' => $this->t('No reports yet. Generate one.'),
      '#title' => $this->t('Reports in %dir:', ['%dir' => $report_dir]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($file = $this->migrateReportHelper->generate()) {
      $this->messenger()->addStatus($this->t('Generated report: %report.', [
        '%report' => $file,
      ]));
    }
    else {
      $this->messenger()->addError($this->t('Error generating report.'));
    }
  }

}
