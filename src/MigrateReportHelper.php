<?php

namespace Drupal\migrate_report;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\StreamOutput;

/**
 * Provides helpers for migrate report module.
 */
class MigrateReportHelper {

  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file-system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * The migration plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPlugiManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * MigrateReportHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file-system service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key-value store factory.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, KeyValueFactoryInterface $key_value_factory, MigrationPluginManagerInterface $migration_plugin_manager, Connection $database) {
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->keyValue = $key_value_factory->get('migrate_last_imported');
    $this->migrationPlugiManager = $migration_plugin_manager;
    $this->database = $database;
  }

  /**
   * Checks if a report can be generated.
   *
   * @param string|null $path
   *   (optional) The destination path. If not passed the configured path will
   *   be used.
   *
   * @return true|string[]
   *   Either TRUE, if the report can be generated, or a list of reasons for not
   *   being able to generate the report.
   */
  public function canGenerate($path = NULL) {
    $reasons = [];

    $config = $this->configFactory->get('migrate_report.config');
    $report_dir = $path ?: $config->get('report_dir');
    if (!$this->fileSystem->prepareDirectory($report_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $reasons[] = $this->t("Directory %dir doesn't exist or is not writable.", [
        '%dir' => $report_dir,
      ]);
    }

    if (!$this->keyValue->getAll()) {
      $reasons[] = t('No migration has been executed yet.');
    }

    return empty($reasons) ? TRUE : $reasons;
  }

  /**
   * Generates a new report based on the last migration run.
   *
   * @param string|null $path
   *   (optional) The destination path. If not passed the configured path will
   *   be used.
   *
   * @return string|null
   *   The generated file or NULL on error.
   */
  public function generate($path = NULL): ?string {
    $last_imported = $this->keyValue->getAll();

    asort($last_imported);
    $migration_list = array_keys($last_imported);

    $last_run = max($last_imported) / 1000;

    $migrations = $this->migrationPlugiManager->createInstances('');
    ksort($migrations);

    $path = $path ?: $this->configFactory->get('migrate_report.config')->get('report_dir');
    $file = rtrim($path, '/\/');
    $file .= DIRECTORY_SEPARATOR . date('Y-m-d H:i', $last_run) . '.txt';

    $table = (new Table(new StreamOutput(fopen($file, 'w'))))
      ->setHeaders([t('Source ID(s)'), t('Message')]);

    $count = 1;
    foreach ($migrations as $migration_id => $migration) {
      if (!$migrated = in_array($migration_id, $migration_list)) {
        $label = $this->t('@label (@id) not migrated yet', [
          '@label' => $migration->label(),
          '@id' => $migration_id,
        ]);
      }
      else {
        $id_map = $migration->getIdMap();
        $imported = $id_map->importedCount();
        $source_plugin = $migration->getSourcePlugin();
        $source_rows = $source_plugin->count();
        if ($source_rows == -1) {
          $source_rows = $this->t('N/A');
          $unprocessed = $this->t('N/A');
        }
        else {
          $unprocessed = $source_rows - $id_map->processedCount();
        }

        $label = $this->t('@label (@id) on @time: @total total, @imported imported, @unprocessed unprocessed', [
          '@label' => $migration->label(),
          '@id' => $migration_id,
          '@time' => date('Y-m-d H:i', $last_imported[$migration_id] / 1000),
          '@total' => $source_rows,
          '@imported' => $imported,
          '@unprocessed' => $unprocessed,
        ]);
      }

      $cell = new TableCell((string) $label, ['colspan' => 2]);
      $table->addRow([$cell]);

      if ($migrated) {
        $keys = [];
        $delta = 1;
        foreach ($source_plugin->getIds() as $id => $info) {
          $keys[$id] = 'sourceid' . $delta++;
        }

        $message_table = $id_map->messageTableName();
        $map_table = $id_map->mapTableName();

        $query = $this->database->select($message_table, 'msg');
        $query->leftJoin($map_table, 'map', 'msg.source_ids_hash=map.source_ids_hash');
        $query->fields('msg', ['level', 'message'])
          ->fields('map', array_values($keys));
        $rows = $query
          ->execute()
          ->fetchAll();

        if ($rows) {
          foreach ($rows as $item) {
            $ids = [];
            $first_key = reset($keys);
            if ($item->$first_key !== NULL) {
              foreach ($keys as $key) {
                $ids[] = $item->$key;
              }
            }
            $message = preg_replace('| \([^)].*:\d+\)$|', '', $item->message);
            $table->addRow([implode("\n", $ids), wordwrap($message)]);
          }
        }
        else {
          $table->addRow(
            [
              new TableCell((string) $this->t('No errors.'), [
                'colspan' => 2,
              ]),
            ],
          );
        }
      }

      if ($count < count($migrations)) {
        $table->addRow(new TableSeparator());
      }

      $count++;
    }
    $table->render();

    return $file;
  }

}
