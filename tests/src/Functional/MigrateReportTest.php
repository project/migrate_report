<?php

namespace Drupal\Tests\migrate_report\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests Migrate Report module.
 *
 * @group migrate_report
 */
class MigrateReportTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'migrate_report_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests module UI functionality.
   */
  public function testFunctionality(): void {
    $assert = $this->assertSession();
    $this->drupalLogin($this->createUser(['access site reports']));
    $file_system = $this->container->get('file_system');
    $reports_path = $this->config('migrate_report.config')->get('report_dir');

    $this->drupalGet('/admin/reports/migrate');

    // Check that a proper warning is displayed.
    $assert->pageTextContains('The report cannot be generated due to following reasons:');
    $assert->pageTextContains('No migration has been executed yet.');
    $assert->pageTextContains('No reports yet. Generate one.');
    $reports = $file_system->scanDirectory($reports_path, '/\.txt$/');
    $this->assertEmpty($reports);

    $this->runMigration();

    $this->drupalGet('/admin/reports/migrate');
    $assert->pageTextNotContains('The report cannot be generated due to following reasons:');
    $assert->pageTextNotContains('No migration has been executed yet.');
    $assert->pageTextContains('No reports yet. Generate one.');

    $this->getSession()->getPage()->pressButton('Generate');

    $report = file_get_contents(key($file_system->scanDirectory($reports_path, '/\.txt$/')));
    $this->assertStringContainsString('(this_is_a_very_very_very_but_very_long_plugin_id)', $report);
    $this->assertStringContainsString('2 total, 2 imported, 0 unprocessed', $report);
    $this->assertStringContainsString('No errors.', $report);
  }

  /**
   * Runs a test migration.
   */
  protected function runMigration() {
    $this->drush('migrate:import', ['this_is_a_very_very_very_but_very_long_plugin_id']);

    // Tests the migration succeeded.
    $entity1 = EntityTest::load(1);
    $this->assertEquals(1, $entity1->id());
    $this->assertSame('Name 1', $entity1->label());
    $entity2 = EntityTest::load(2);
    $this->assertEquals(2, $entity2->id());
    $this->assertSame('Name 2', $entity2->label());
  }

}
